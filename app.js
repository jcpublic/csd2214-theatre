// PART 1:  Calculating the ticket price
// 1. how many seats did the user select?
// 2. what movie did they choose? how much is it?
// 3. do the calculation and display how much the final cost is


// PART 2:  UI - Showing if the seats are occupied or not

const TICKET_PRICE = 10;

let calculatePrice = function() {
    // 1. get all selected seats elements
    let selectedSeats = document.querySelectorAll(".row .seat.selected");
    console.log(selectedSeats);

    // 2. count number of selected seats
    let numSeats = selectedSeats.length;

    // 3. get ticket price
    // @TODO: This should come from the <select> dropdown

    // 4. compute total
    let totalCost = numSeats * TICKET_PRICE;
    console.log("Total Cost: $" + totalCost);

    // 5. Update UI
    document.querySelector("#sp-seat-count").innerText = numSeats;
    document.querySelector("#sp-total-price").innerText = "$" + totalCost;
}
let updateSeatUI = function(event) {
    console.log("clicked");
    console.log(event);

    // get the element the person clicked on
    let element = event.target;

    // check if the element was a seat
    if (element.classList.contains("seat")) {
        // if yes, then decide if seat is occupied or not

        console.log("you clicked on a seat");

        if (element.classList.contains("occupied")) {
            console.log("sorry, seat is occupied!");
            return;
        }
        else {
            element.classList.toggle("selected");
            calculatePrice();
        }
        
    }
    else {
        // if no, then ignore
        console.log("you clicked somewhere else");
    }
}

let buySeatsPressed = function() {

    console.log("BUYING SEATS!");
    // 1. save selected seats to localStorage
    let selectedSeatPositions = [];
    let seats = document.querySelectorAll(".row .seat");
    console.log(seats);
    for (let i = 0; i < seats.length; i++) {
        // if seat is occupied, record the index of that seat
        let seat = seats[i];
        if (seat.classList.contains("selected")) {
            selectedSeatPositions.push(i);
            console.log("seat with selected found. appending to arry");
        }
        else {
            console.log("seat does not have selected class");
            console.log(seat);
        }
    }
    
    //@DEBUG: show positions of selected seats in console
    console.log(selectedSeatPositions);    

    // save to local storage
    localStorage.setItem("selectedPositions", JSON.stringify(selectedSeatPositions));

    // 2. @FUTURE-ENHANCEMENT: Update all selected seats to "occupied" & save to localStorage.
}
document.querySelector(".movie-theatre").addEventListener("click", updateSeatUI);
document.querySelector("#btn-buy").addEventListener("click", buySeatsPressed);


